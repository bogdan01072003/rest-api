package com.example.dt.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

public class Database implements Serializable {
  Map<String, Table> tables;

  public Database() {
    this.tables = new HashMap<>();
  }

  public void createTable(String tableName) {
    Table table = new Table(tableName);
    tables.put(tableName, table);
  }

  public void dropTable(String tableName) {
    tables.remove(tableName);
  }

  public void insertRow(String tableName, int rowId, Map<String, Field> row) {
    Table table = tables.get(tableName);
    if (table != null) {
      table.insertRow(rowId, row);
    }
  }

  public void deleteRow(String tableName, int rowId) {
    Table table = tables.get(tableName);
    if (table != null) {
      table.deleteRow(rowId);
    }
  }

  public Map<String, Field> getRow(String tableName, int rowId) {
    Table table = tables.get(tableName);
    if (table != null) {
      return table.getRow(rowId);
    }
    return null;
  }

  public void updateRow(String tableName, int rowId, Map<String, Field> newRow) {
    Table table = tables.get(tableName);
    if (table != null) {
      table.updateRow(rowId, newRow);
    }
  }
  public Map<String, Field> getRowDifference(String tableName1, String tableName2, int rowId) {
    Map<String, Field> row1 = tables.get(tableName1).getRow(rowId);
    Map<String, Field> row2 = tables.get(tableName2).getRow(rowId);

    Map<String, Field> difference = new HashMap<>();

    for (Map.Entry<String, Field> entry : row1.entrySet()) {
      String fieldName = entry.getKey();
      Field field1 = entry.getValue();
      Field field2 = row2.get(fieldName);

      if (field2 == null || !field1.equals(field2)) {
        difference.put(fieldName, field1);
      }
    }

    for (Map.Entry<String, Field> entry : row2.entrySet()) {
      String fieldName = entry.getKey();
      if (!row1.containsKey(fieldName)) {
        difference.put(fieldName, entry.getValue());
      }
    }

    return difference;
  }

  public Map<String, Table> getTables() {
    return tables;
  }
  public void removeDuplicatesFromTable(String tableName) {
    Table table = tables.get(tableName);
    if (table != null) {
      table.removeDuplicates();
    }
  }

  public void saveToDisk(String fileName) {
    try (FileOutputStream fileOut = new FileOutputStream(fileName);
        ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {
      objectOut.writeObject(this);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static Database loadFromDisk(String fileName) {
    try (FileInputStream fileIn = new FileInputStream(fileName);
        ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {
      return (Database) objectIn.readObject();
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
      return null;
    }
  }
}

