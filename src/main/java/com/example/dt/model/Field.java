package com.example.dt.model;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

public class  Field implements Serializable {
  String name;
  Object value;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }

  public Field(String name, Object value) {
    this.name = name;
    if (validateValue(value)) {
      this.value = value;
    } else {
      System.out.println(value+" Not correct");
    }
  }

  private boolean validateValue(Object value) {
    if (value instanceof Integer ||
        value instanceof Double ||
        value instanceof String ||
        value instanceof Character ||
        value instanceof DataLnvl ||
        value instanceof Date) {

      return true;
    }
    return false;
  }

  private String getFileExtension(File file) {
    String extension = null;
    String fileName = file.getName();
    int dotIndex = fileName.lastIndexOf('.');
    if (dotIndex > 0 && dotIndex < fileName.length() - 1) {
      extension = fileName.substring(dotIndex + 1).toLowerCase();
    }
    return extension;
  }
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Field field = (Field) obj;
    if (!name.equals(field.name)) {
      return false;
    }
    if (value == null) {
      return field.value == null;
    } else {
      return value.equals(field.value);
    }
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + (value != null ? value.hashCode() : 0);
    return result;
  }
}
