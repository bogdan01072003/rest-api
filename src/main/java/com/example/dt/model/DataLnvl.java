package com.example.dt.model;

import java.io.Serializable;
import java.util.Date;

public class DataLnvl implements Serializable {
  private Date start;
  private Date end;

  public DataLnvl() {

  }

  public DataLnvl(Date start, Date end) {
    this.start = start;
    this.end = end;
  }

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }
}