package com.example.dt.model;

import com.example.dt.sereliser.TableSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
@JsonSerialize(using = TableSerializer.class)
public class Table implements Serializable {
  public String name;
  public Map<Integer, Map<String, Field>> rows;

  public Table(String name) {
    this.name = name;
    this.rows = new HashMap<>();
  }

  public void insertRow(int rowId, Map<String, Field> row) {
    rows.put(rowId, row);
    updatePreviousRowsWithNewField(row);
  }

  private void updatePreviousRowsWithNewField(Map<String, Field> newRow) {
    for (Map<String, Field> existingRow : rows.values()) {
      for (Map.Entry<String, Field> newFieldEntry : newRow.entrySet()) {
        String fieldName = newFieldEntry.getKey();
        Field newField = newFieldEntry.getValue();

        if (!existingRow.containsKey(fieldName)) {
          existingRow.put(fieldName, new Field(fieldName, ""));
        }
      }
    }
  }
  public void removeDuplicates() {
    Set<Map<String, Field>> uniqueRows = new HashSet<>();
    Set<Integer> duplicateRowIds = new HashSet<>();

    for (Map.Entry<Integer, Map<String, Field>> entry : rows.entrySet()) {
      Map<String, Field> row = entry.getValue();

      if (!uniqueRows.add(row)) {
        duplicateRowIds.add(entry.getKey());
      }
    }

    for (int rowId : duplicateRowIds) {
      deleteRow(rowId);
    }
  }
  public void deleteRow(int rowId) {
    rows.remove(rowId);
  }

  public Map<String, Field> getRow(int rowId) {
    return rows.get(rowId);
  }

  public void updateRow(int rowId, Map<String, Field> newRow) {
    rows.put(rowId, newRow);
  }
}
