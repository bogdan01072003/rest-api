package com.example.dt.service;

import com.example.dt.model.Database;
import com.example.dt.model.Table;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Service;

@Service
public class ServiceBD {
  public Set<String> getAllTables()
  {
     Database a = new Database();
     a.loadFromDisk("C:\\Users\\bogda\\Music\\dt\\database.ser");
     return a.getTables().keySet();
  }
}
