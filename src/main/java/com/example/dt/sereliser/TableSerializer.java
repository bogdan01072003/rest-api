package com.example.dt.sereliser;

import com.example.dt.model.Field;
import com.example.dt.model.Table;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.util.Map;

public class TableSerializer extends JsonSerializer<Table> {

  @Override
  public void serialize(Table table, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
    jsonGenerator.writeStartObject();

    // Серіалізуємо поле 'name'
    jsonGenerator.writeStringField("name", table.name);

    // Серіалізуємо поле 'rows'
    jsonGenerator.writeFieldName("rows");
    jsonGenerator.writeStartObject();
    for (Map.Entry<Integer, Map<String, Field>> entry : table.rows.entrySet()) {
      int rowId = entry.getKey();
      Map<String, Field> row = entry.getValue();

      jsonGenerator.writeFieldName(Integer.toString(rowId));
      jsonGenerator.writeStartObject();

      for (Map.Entry<String, Field> rowEntry : row.entrySet()) {
        String fieldName = rowEntry.getKey();
        Field field = rowEntry.getValue();

        // Серіалізуємо поля рядка
        jsonGenerator.writeObjectField(fieldName, field);
      }

      jsonGenerator.writeEndObject();
    }
    jsonGenerator.writeEndObject();

    jsonGenerator.writeEndObject();
  }
}

