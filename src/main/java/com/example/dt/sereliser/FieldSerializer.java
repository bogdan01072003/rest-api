package com.example.dt.sereliser;

import com.example.dt.model.DataLnvl;
import com.example.dt.model.Field;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.util.Date;

public class FieldSerializer extends JsonSerializer<Field> {

  @Override
  public void serialize(Field field, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
    jsonGenerator.writeStartObject();

    // Серіалізуємо поля класу Field
    jsonGenerator.writeStringField("name", field.getName());
    // Щоб відобразити різні типи значень, можна використовувати if-else або використовувати instanceof для конкретних типів значень
    if (field.getValue() instanceof Integer) {
      jsonGenerator.writeNumberField("value", (Integer) field.getValue());
    } else if (field.getValue() instanceof Double) {
      jsonGenerator.writeNumberField("value", (Double) field.getValue());
    } else if (field.getValue() instanceof String) {
      jsonGenerator.writeStringField("value", (String) field.getValue());
    } else if (field.getValue() instanceof Character) {
      jsonGenerator.writeStringField("value", String.valueOf(field.getValue()));
    } else if (field.getValue() instanceof DataLnvl) {
      // Якщо тип поля DataLnvl, серіалізуємо його відповідно до вашого власного типу
      // Зробіть те, що потрібно для серіалізації DataLnvl
    } else if (field.getValue() instanceof Date) {
      // Якщо тип поля Date, серіалізуємо його відповідно до вашого власного типу
      // Зробіть те, що потрібно для серіалізації Date
    }
    // Додайте інші типи, які ви хочете серіалізувати

    jsonGenerator.writeEndObject();
  }
}

