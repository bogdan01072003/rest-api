package com.example.dt.contrller;

import com.example.dt.model.Database;
import com.example.dt.model.Field;
import com.example.dt.model.Table;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/database")
public class DatabaseController {

  private final Database database;

  public DatabaseController() {
  }

  @GetMapping("/tables")
  public Map<String, Table> getAllTables() {
    return database.getTables();
  }

  @PostMapping("/tables/{tableName}")
  public void createTable(@PathVariable String tableName) {
    database.createTable(tableName);
  }

  @DeleteMapping("/tables/{tableName}")
  public void dropTable(@PathVariable String tableName) {
    database.dropTable(tableName);
  }

  @GetMapping("/tables/{tableName}/rows/{rowId}")
  public Map<String, Field> getRow(@PathVariable String tableName, @PathVariable int rowId) {
    return database.getRow(tableName, rowId);
  }

  @PostMapping("/tables/{tableName}/rows/{rowId}")
  public void insertRow(
      @PathVariable String tableName,
      @PathVariable int rowId,
      @RequestBody Map<String, Field> row) {
    database.insertRow(tableName, rowId, row);
  }

  @DeleteMapping("/tables/{tableName}/rows/{rowId}")
  public void deleteRow(@PathVariable String tableName, @PathVariable int rowId) {
    database.deleteRow(tableName, rowId);
  }

  @PutMapping("/tables/{tableName}/rows/{rowId}")
  public void updateRow(
      @PathVariable String tableName,
      @PathVariable int rowId,
      @RequestBody Map<String, Field> newRow) {
    database.updateRow(tableName, rowId, newRow);
  }
  @PostMapping("/tables/{tableName}/removeDuplicates")
  public void removeDuplicatesFromTable(@PathVariable String tableName) {
    database.removeDuplicatesFromTable(tableName);
  }
}

